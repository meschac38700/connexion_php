<?php

class PDO
{

    private $bdd;

    public function __construct()
    {
        $this->bdd = new SQLite3('database.db');
    }

    public function select( $request )
    {
        $results = $this->bdd->query($request);
        return $results->fetchArray();
    }

    public function prepare_select($request, $data)
    {
        $statement = $this->bdd->prepare($request);
        foreach($data as $key => $value)
        {
            $statement->bindValue(':'.$key, $value);
        }
        $results = $statement->execute();
        return $results->fetchArray();
    }

    // source code http://www.sqlitetutorial.net/sqlite-php/insert/
    public function prepare_insert_user( $request, $data ) {
        
        $statement = $this->bdd->prepare( $request );
        foreach($data as $key => $value)
        {
            $statement->bindValue(':'.$key, $value);
        }
        $statement->execute();
        return $this->bdd->lastInsertId();

    }

}