<?php
session_start();
require "Utilities.php";

if ( !empty($_SESSION['user']))
{
	header("Location: profil.php");
}

$errors = [];

$error_msg = !empty($_SESSION['error'])?"Veuillez vous identifier d'abord !":"";

if( isset($_POST['connexion']) )
{
	extract($_POST);

	if ( empty($id) )
	{
		$errors['id'] = "Veuillez renseigné l'identifiant";
	}
	elseif( strlen($id) < 3 )
	{
		$errors['id'] = "Votre identifiant doit comporter au moins 3 caracteres !";
	}

	if ( empty($pwd) )
	{
		$errors['pwd'] = "Veuillez renseigné le mot de passe";
	}
	elseif( strlen($pwd) < 6)
	{
		$errors['pwd'] = "Votre mot de passe doit comporter au moins 6 caractères ! ";
	}

	if( empty($errors) ) 
	{
		$_SESSION['user']['username'] = $id;
		$_SESSION['user']['password'] = $pwd;
		// Possibilité de faire une connexion à la base de données et de check les informations de l'utilisateur
		// est ce que l'utilisateur existe ?
		// est ce que le mdp correspond à l'identifiant saisi ? etc...
		header("Location: profil.php");
	}



}
require "connexion.view.php";
